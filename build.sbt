name := "practice-twitter"

version := "0.0.1"

scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.16"
lazy val akkaGrpcVersion = "0.4"

enablePlugins(AkkaGrpcPlugin)

// ALPN agent
enablePlugins(JavaAgent)
javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.7" % "runtime;test"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence-query" % akkaVersion,
  "org.elasticsearch.client" % "elasticsearch-rest-client" % "6.4.2",
  "com.lightbend.akka" %% "akka-stream-alpakka-elasticsearch" % "1.0-M1",
  "com.typesafe.akka" %% "akka-remote" % akkaVersion,
  "org.iq80.leveldb" % "leveldb" % "0.7",
  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test",
)
