package com.mokamoto12.twitter.domain.event

import com.mokamoto12.twitter.domain.model.tweet.{Message, TweetId}

case class TweetTweeted(tweetId: TweetId, message: Message) extends Event
