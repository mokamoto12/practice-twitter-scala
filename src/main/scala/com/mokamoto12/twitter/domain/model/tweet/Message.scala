package com.mokamoto12.twitter.domain.model.tweet

object Message {
  def empty = new Message("")
}

case class Message(message: String) {
  require(message.length <= 140)
}
