package com.mokamoto12.twitter.domain.model.tweet

import akka.actor._
import akka.persistence._

object Tweet {
  def props(tweetId: TweetId) = Props(new Tweet(tweetId))

  case class NewTweet(message: Message)

  case class UpdateMessage(message: Message)

  case object DeleteTweet

  trait Event

  case class TweetCreated(message: Message) extends Event

  case class MessageUpdated(message: Message) extends Event

  case object TweetDeleted extends Event

}

class Tweet(tweetId: TweetId) extends PersistentActor {

  import Tweet._

  override def persistenceId: String = tweetId.id

  private var initialized = false

  private var message: Message = Message.empty

  override def receiveRecover: Receive = {
    case TweetCreated(message: Message) =>
      initialized = true
      this.message = message
    case MessageUpdated(message: Message) => this.message = message
  }

  override def receiveCommand: Receive = {
    case NewTweet(message: Message) => if (!initialized) persist(TweetCreated(message)) { _ => this.message = message }
    case UpdateMessage(message: Message) => if (initialized) persist(MessageUpdated(message)) { _ => this.message = message }
    case DeleteTweet => if (initialized) persist(TweetDeleted) { _ => initialized = false}
  }
}
