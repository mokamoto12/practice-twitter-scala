package com.mokamoto12.twitter.domain.model.tweet

trait TweetRepository {
  def nextIdentity(): TweetId
  def findById(tweetId: TweetId): Tweet
}
