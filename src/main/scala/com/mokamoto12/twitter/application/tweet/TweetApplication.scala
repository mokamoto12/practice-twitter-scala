package com.mokamoto12.twitter.application.tweet

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.AskableActorRef
import akka.pattern.ask
import akka.pattern.pipe
import akka.util.Timeout

import scala.concurrent.duration._
import com.mokamoto12.twitter.domain.model.tweet.{Message, Tweet, TweetId, TweetRepository}

import scala.concurrent.ExecutionContext

object TweetApplication {
  def props(tweetRepository: TweetRepository) = Props(new TweetApplication(tweetRepository))

  case class TweetCommand(message: Message) extends Command

  case class UpdateMessageCommand(tweetId: TweetId, message: Message) extends Command

  case class DeleteTweetCommand(tweetId: TweetId) extends Command
}

class TweetApplication(tweetRepository: TweetRepository) extends Actor with ActorLogging {

  import TweetApplication._

  implicit val ex: ExecutionContext = context.dispatcher
  implicit val timeout: Timeout = Timeout(10.seconds)

  override def receive: Receive = {
    case TweetCommand(message: Message) =>
      log.info(s"TweetCommand $message")
      val tweetId: TweetId = tweetRepository.nextIdentity()
      val tweet: ActorRef = context.actorOf(Tweet.props(tweetId))
      tweet ! Tweet.NewTweet(message)
      sender() ! tweetId.id

    case UpdateMessageCommand(tweetId: TweetId, message: Message) =>
      log.info(s"UpdateMessageCommand $tweetId $message")
      val tweet: ActorRef = context.actorOf(Tweet.props(tweetId))
      tweet ! Tweet.UpdateMessage(message)
      sender() ! tweetId.id

    case DeleteTweetCommand(tweetId: TweetId) =>
      log.info(s"DeleteTweetCommand $tweetId")
      val tweet: ActorRef = context.actorOf(Tweet.props(tweetId))
      tweet ! Tweet.DeleteTweet
      sender() ! tweetId.id
  }
}
