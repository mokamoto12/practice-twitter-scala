package com.mokamoto12.twitter.infrastructure.projection.elasticsearch

import akka.NotUsed
import akka.actor.ActorSystem
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.stream.ActorMaterializer
import akka.stream.alpakka.elasticsearch.WriteMessage._
import akka.stream.alpakka.elasticsearch.scaladsl.ElasticsearchFlow
import akka.stream.scaladsl.{Sink, Source}
import com.mokamoto12.twitter.domain.model.tweet.Message
import com.mokamoto12.twitter.domain.model.tweet.Tweet.{MessageUpdated, TweetCreated, TweetDeleted}
import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import spray.json.DefaultJsonProtocol._
import spray.json._

object ElasticsearchTweetProjection {
  implicit val tweetFormat: JsonFormat[Message] = jsonFormat1(Message.apply)
}

class ElasticsearchTweetProjection(system: ActorSystem) {

  import ElasticsearchTweetProjection._

  def run(): Unit = {
    implicit val client: RestClient = RestClient.builder(new HttpHost("localhost", 9200)).build()

    implicit val mat: ActorMaterializer = ActorMaterializer()(system)
    val queries = PersistenceQuery(system).readJournalFor[LeveldbReadJournal](
      LeveldbReadJournal.Identifier)

    val src: Source[EventEnvelope, NotUsed] = queries.eventsByTag("DomainEvent")
    src
      .map { ee: EventEnvelope =>
        system.log.info(ee.toString)
        ee.event match {
          case event: TweetCreated =>
            createIndexMessage(ee.persistenceId, event.message)

          case event: MessageUpdated =>
            createUpsertMessage(ee.persistenceId, event.message)

          case TweetDeleted =>
            createDeleteMessage[Message](ee.persistenceId)
        }
      }
      .via(ElasticsearchFlow.create[Message]("tweet", "_doc"))
      .runWith(Sink.seq)
  }
}
