package com.mokamoto12.twitter.infrastructure.eventadapter

import akka.persistence.journal.{Tagged, WriteEventAdapter}
import com.mokamoto12.twitter.domain.model.tweet.Tweet.Event

class TweetEventAdapter extends WriteEventAdapter {
  override def manifest(event: Any): String = ""

  override def toJournal(event: Any): Any = event match {
    case _: Event =>
      println(s"tag to $event")
      Tagged(event, Set("DomainEvent"))
    case _ => event
  }
}
