package com.mokamoto12.twitter.infrastructure.persistence.leveldb

import java.util.UUID.randomUUID
import com.mokamoto12.twitter.domain.model.tweet.{Tweet, TweetId, TweetRepository}

class LevelDBTweetRepository extends TweetRepository {
  override def nextIdentity(): TweetId = TweetId(randomUUID().toString)

  override def findById(tweetId: TweetId): Tweet = ???
}
