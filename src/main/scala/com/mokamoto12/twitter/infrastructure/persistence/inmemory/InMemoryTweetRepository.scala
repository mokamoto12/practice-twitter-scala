package com.mokamoto12.twitter.infrastructure.persistence.inmemory

import java.util.UUID.randomUUID
import com.mokamoto12.twitter.domain.model.tweet.{Tweet, TweetId, TweetRepository}

class InMemoryTweetRepository extends TweetRepository {
  private val map = Map[TweetId, Tweet]()

  override def nextIdentity(): TweetId = TweetId(randomUUID().toString)

  override def findById(tweetId: TweetId): Tweet = map(tweetId)
}
