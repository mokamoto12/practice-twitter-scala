package com.mokamoto12.twitter.interfaces.grpc

import akka.pattern.ask
import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.util.Timeout

import scala.concurrent.duration._
import com.mokamoto12.twitter.application.tweet.TweetApplication
import com.mokamoto12.twitter.domain.model.tweet.{Message, TweetId}
import com.mokamoto12.twitter.infrastructure.persistence.inmemory.InMemoryTweetRepository

import scala.concurrent.Future

class TweetServiceImpl(materializer: Materializer, system: ActorSystem) extends TweetService {

  import materializer.executionContext

  private implicit val timeout: Timeout = Timeout(10.seconds)
  private implicit val mat: Materializer = materializer
  private val tweetApplication = system.actorOf(TweetApplication.props(new InMemoryTweetRepository()))

  override def tweet(in: TweetRequest): Future[TweetReply] = {
    (tweetApplication ? TweetApplication.TweetCommand(Message(in.message))).mapTo[String].map(TweetReply(_, in.message))
  }

  override def updateMessage(in: UpdateMessageRequest): Future[TweetReply] = {
    (tweetApplication ? TweetApplication.UpdateMessageCommand(TweetId(in.tweetId), Message(in.message))).mapTo[String].map(_ => TweetReply(in.tweetId, in.message))
  }

  override def deleteTweet(in: DeleteTweetRequest): Future[TweetIdReply] = {
    (tweetApplication ? TweetApplication.DeleteTweetCommand(TweetId(in.tweetId))).mapTo[String].map(TweetIdReply(_))
  }
}
